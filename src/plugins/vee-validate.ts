import { extend } from "vee-validate";
import { required, max } from "vee-validate/dist/rules";

extend("required", {
  ...required,
  message: "This field is required"
});

extend("max", {
  ...max,
  message: (fieldName, params) =>
    `This field can't exceed maximum number of ${params.length} characters.`
});
