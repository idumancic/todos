import Vue from "vue";
import Vuex from "vuex";
import { TodoState } from "./modules/todos";

Vue.use(Vuex);

export interface RootState {
  todos: TodoState;
}

export default new Vuex.Store<RootState>({});
