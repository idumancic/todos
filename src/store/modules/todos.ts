import { Todo } from "@/types";
import store from "../index";
import {
  VuexModule,
  Module,
  Mutation,
  Action,
  getModule
} from "vuex-module-decorators";

export interface TodoState {
  items: Todo[];
}

@Module({ dynamic: true, store, name: "todos" })
class Todos extends VuexModule implements TodoState {
  items: Todo[] = [];

  get nextItemId() {
    if (this.items.length === 0) return 1;
    return Math.max(...this.items.map(x => x.id)) + 1;
  }

  get completedItems() {
    return this.items.filter(x => x.completed === true);
  }

  get todoItems() {
    return this.items.filter(x => x.completed === false);
  }

  @Mutation
  private REMOVE_ITEM(itemIndex: number) {
    this.items.splice(itemIndex, 1);
  }

  @Mutation
  private UPDATE_ITEM(payload: Todo & { itemIndex: number }) {
    const current = this.items[payload.itemIndex];
    current.value = payload.value;
    current.completed = payload.completed;

    if (payload.inEdit !== undefined) {
      delete payload.inEdit;
    }
  }

  @Mutation
  private ADD_ITEM(todo: Todo) {
    this.items.push(todo);
  }

  @Action
  removeItem(todoId: number) {
    const itemIndex = this.items.findIndex(x => x.id === todoId);

    if (itemIndex === -1) {
      return;
    }

    this.REMOVE_ITEM(itemIndex);
  }

  @Action
  updateItem(todo: Todo) {
    const itemIndex = this.items.findIndex(x => x.id === todo.id);

    if (itemIndex === -1) {
      return;
    }

    this.UPDATE_ITEM({ itemIndex, ...todo });
  }

  @Action
  addItem(todoValue: string) {
    const todo: Todo = {
      id: this.nextItemId,
      value: todoValue,
      completed: false
    };

    this.ADD_ITEM(todo);
  }
}

export const todosModule = getModule(Todos);
